package io.overcoded.repository.annotation;

import java.lang.annotation.*;

/**
 * The outcome is one generated methods in the public JpaRepository of the domain class compile time.
 * 'findOneBy{methodName}({types...})'returning an Optional value.
 * Only with @DynamicRepository annotated classes.
 *
 * @author Adam Belak
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.SOURCE)
@Repeatable(value = FindOneArray.class)
public @interface FindOne {
    /**
     * defines suffix for findOne methods
     *
     * @return string
     */
    String method() default "";

    /**
     * defines method argument types
     *
     * @return Class array
     */
    Class<?>[] types() default {};

}

package io.overcoded.repository.annotation.processor.supplier;

import io.overcoded.repository.annotation.processor.domain.ElementCollection;
import io.overcoded.repository.annotation.processor.domain.RepositoryData;

/**
 * @author Diana Ladanyi
 */
public interface Supplier {
    Supplier getLast();

    Supplier getNext();

    void setNext(Supplier supplier);

    RepositoryData execute(ElementCollection elementCollection, RepositoryData repositoryData);
}

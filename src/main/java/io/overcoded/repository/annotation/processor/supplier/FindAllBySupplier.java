package io.overcoded.repository.annotation.processor.supplier;

import io.overcoded.repository.annotation.FindAllBy;
import io.overcoded.repository.annotation.processor.config.RepositoryProperties;
import io.overcoded.repository.annotation.processor.domain.ElementCollection;
import io.overcoded.repository.annotation.processor.domain.MethodData;
import io.overcoded.repository.annotation.processor.domain.RepositoryData;

import javax.lang.model.element.Element;
import java.util.Objects;

/**
 * @author Diana Ladanyi
 * @author Adam Belak
 */
class FindAllBySupplier extends AbstractSupplier implements Supplier {
    private static final String FIND_ALL_METHOD_PREFIX = "AllBy";
    private static final String COUNT_ALL_METHOD_PREFIX = "countAllBy";
    private static final String METHOD_PARAMETER_SUFFIX = " arg0";

    public FindAllBySupplier(RepositoryProperties repositoryConfiguration) {
        super(repositoryConfiguration);
    }

    @Override
    public RepositoryData execute(ElementCollection elementCollection, RepositoryData repositoryData) {
        if (Objects.nonNull(elementCollection.getFindAllByElements()) && !elementCollection.getFindAllByElements().isEmpty()) {
            elementCollection.getFindAllByElements()
                    .forEach(findAllElement -> addFindAllByAnnotation(repositoryData, findAllElement));
        }
        return super.execute(elementCollection, repositoryData);
    }

    private void addFindAllByAnnotation(RepositoryData repositoryData, Element findAllByElement) {
        repositoryData.getFindMethods().add(createMethodData(findAllByElement, FIND_ALL_METHOD_PREFIX));
        repositoryData.getCountMethods().add(createMethodData(findAllByElement, COUNT_ALL_METHOD_PREFIX));
    }

    private MethodData createMethodData(Element findAllByElement, String findAllMethodPrefix) {
        String typeName = getTypeName(findAllByElement);
        return MethodData.builder()
                .methodName(createMethod(findAllMethodPrefix, findAllByElement))
                .parameters(typeName + METHOD_PARAMETER_SUFFIX)
                .build();
    }

    private String getTypeName(Element findAllByElement) {
        String typeName = findAllByElement
                .asType()
                .toString()
                .replace("java.lang.", "");
        if (typeName.contains("@") && typeName.contains(" ")) {
            typeName = typeName.substring(typeName.lastIndexOf(" ")).trim();
        }
        return typeName;
    }

    private String createMethod(String prefix, Element findAllByElement) {
        String elementName = findAllByElement.getSimpleName().toString();
        FindAllBy findAllBy = findAllByElement.getAnnotation(FindAllBy.class);
        String modifier = Objects.nonNull(findAllBy) ? findAllBy.modifier() : "";
        return prefix + elementName.substring(0, 1).toUpperCase() + elementName.substring(1) + modifier;
    }
}

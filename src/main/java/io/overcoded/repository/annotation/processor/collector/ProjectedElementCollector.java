package io.overcoded.repository.annotation.processor.collector;

import io.overcoded.repository.annotation.processor.domain.ElementCollection;
import lombok.extern.slf4j.Slf4j;

import javax.lang.model.element.Element;
import java.util.List;
import java.util.function.Function;

/**
 * @author Adam Belak
 */
@Slf4j
public class ProjectedElementCollector extends AbstractElementCollector implements ElementCollector {
    @Override
    protected Function<ElementCollection, List<Element>> extract() {
        return ElementCollection::getProjectedElements;
    }

    @Override
    protected String getKey(Element element) {
        return element.getEnclosingElement().getSimpleName().toString();
    }
}

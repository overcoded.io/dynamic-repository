package io.overcoded.repository.annotation.processor.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Diana Ladanyi
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MethodData {
    private String methodName;
    private String parameters;
}

package io.overcoded.repository.annotation.processor.supplier;

import io.overcoded.repository.annotation.processor.config.RepositoryProperties;
import io.overcoded.repository.annotation.processor.config.RepositoryPropertiesReader;

/**
 * @author Diana Ladanyi
 * @author Adam Belak
 */
public class SupplierManagerFactory {
    public static SupplierManager createDefaultManager() {
        RepositoryProperties repositoryConfiguration = RepositoryPropertiesReader.read();
        SupplierChain supplierChain = new SupplierChain();
        supplierChain.addSupplier(new DynamicRepositorySupplier(repositoryConfiguration));
        supplierChain.addSupplier(new FindAllSupplier(repositoryConfiguration));
        supplierChain.addSupplier(new FindAllBySupplier(repositoryConfiguration));
        supplierChain.addSupplier(new FindOneSupplier(repositoryConfiguration));
        supplierChain.addSupplier(new FindOneBySupplier(repositoryConfiguration));
        supplierChain.addSupplier(new ProjectionSupplier(repositoryConfiguration));
        return new SupplierManager(supplierChain);
    }
}

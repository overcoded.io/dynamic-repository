package io.overcoded.repository.annotation.processor;

import com.google.auto.service.AutoService;
import io.overcoded.repository.annotation.processor.collector.ElementCollector;
import io.overcoded.repository.annotation.processor.collector.ElementCollectorFactory;
import io.overcoded.repository.annotation.processor.domain.ElementCollection;
import io.overcoded.repository.annotation.processor.domain.RepositoryData;
import io.overcoded.repository.annotation.processor.generator.SourceCodeWriter;
import io.overcoded.repository.annotation.processor.supplier.SupplierManager;
import io.overcoded.repository.annotation.processor.supplier.SupplierManagerFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.processing.*;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.TypeElement;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * @author Diana Ladanyi
 * @author Adam Belak
 */
@Slf4j
@RequiredArgsConstructor
@AutoService(Processor.class)
@SupportedSourceVersion(SourceVersion.RELEASE_21)
@SupportedAnnotationTypes(value = {
        "io.overcoded.repository.annotation.FindAll",
        "io.overcoded.repository.annotation.FindAllBy",
        "io.overcoded.repository.annotation.FindAllArray",
        "io.overcoded.repository.annotation.FindOne",
        "io.overcoded.repository.annotation.FindOneBy",
        "io.overcoded.repository.annotation.FindOneArray",
        "io.overcoded.repository.annotation.Projected",
        "io.overcoded.repository.annotation.DynamicRepository"})
public class DynamicRepositoryProcessor extends AbstractProcessor {
    private final SupplierManager supplierManager;
    private final SourceCodeWriter sourceCodeWriter;
    private final Map<String, ElementCollector> fillers;

    public DynamicRepositoryProcessor() {
        this(SupplierManagerFactory.createDefaultManager(), new SourceCodeWriter(), ElementCollectorFactory.createElementCollectorMap());
    }

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        Map<String, ElementCollection> elementCollectionMap = getElementCollectionMap(annotations, roundEnv);
        evaluateElementCollectionMap(elementCollectionMap);
        return false;
    }

    private Map<String, ElementCollection> getElementCollectionMap(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        Map<String, ElementCollection> elementCollectionMap = new HashMap<>();
        for (TypeElement annotation : annotations) {
            String annotationName = annotation.getSimpleName().toString();
            if (fillers.containsKey(annotationName)) {
                fillers.get(annotationName).accept(elementCollectionMap, roundEnv.getElementsAnnotatedWith(annotation));
            }
        }
        return elementCollectionMap;
    }

    private void evaluateElementCollectionMap(Map<String, ElementCollection> elementCollectionMap) {
        elementCollectionMap.forEach((entityType, elementCollection) -> {
            if (Objects.nonNull(elementCollection.getDynamicRepositoryElement())) {
                RepositoryData repositoryData = supplierManager.requestSupply(elementCollection, new RepositoryData());
                sourceCodeWriter.generate(repositoryData, processingEnv.getFiler());
            }
        });
    }
}

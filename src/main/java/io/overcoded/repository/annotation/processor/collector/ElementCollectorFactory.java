package io.overcoded.repository.annotation.processor.collector;

import java.util.Map;

/**
 * @author Diana Ladanyi
 * @author Adam Belak
 */
public class ElementCollectorFactory {
    public static Map<String, ElementCollector> createElementCollectorMap() {
        return Map.of(
                "DynamicRepository", new DynamicRepositoryElementCollector(),
                "FindAllArray", new FindAllElementCollector(),
                "FindAll", new FindAllElementCollector(),
                "FindAllBy", new FindAllByElementCollector(),
                "FindOneArray", new FindOneElementCollector(),
                "FindOne", new FindOneElementCollector(),
                "FindOneBy", new FindOneByElementCollector(),
                "Projected", new ProjectedElementCollector()
        );
    }
}

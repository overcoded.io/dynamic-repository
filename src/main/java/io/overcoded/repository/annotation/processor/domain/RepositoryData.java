package io.overcoded.repository.annotation.processor.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

/**
 * @author Diana Ladanyi
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class RepositoryData {
    private String fetchSize;
    private String entityType;
    private String packageName;
    private String simpleClassName;
    private String suffix;
    private String resourcePath;
    private boolean restResourceEnabled;
    private Set<MethodData> findMethods;
    private Set<MethodData> findOneMethods;
    private Set<MethodData> countMethods;
    private Set<MethodData> projectedFields;
}

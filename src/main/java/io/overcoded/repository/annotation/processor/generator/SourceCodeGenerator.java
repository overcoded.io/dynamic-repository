package io.overcoded.repository.annotation.processor.generator;

import io.overcoded.repository.annotation.processor.domain.RepositoryData;

public interface SourceCodeGenerator {
    SourceFile generate(RepositoryData repositoryData);
}

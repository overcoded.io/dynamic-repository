package io.overcoded.repository.annotation.processor.supplier;

import io.overcoded.repository.annotation.processor.domain.ElementCollection;
import io.overcoded.repository.annotation.processor.domain.RepositoryData;

import java.util.Objects;

/**
 * @author Diana Ladanyi
 */
public class SupplierChain {
    private Supplier supplier;

    void addSupplier(Supplier supplier) {
        if (Objects.nonNull(this.supplier)) {
            this.supplier.getLast().setNext(supplier);
        } else {
            this.supplier = supplier;
        }
    }

    RepositoryData execute(ElementCollection elementCollection, RepositoryData repositoryData) {
        return supplier.execute(elementCollection, repositoryData);
    }

}

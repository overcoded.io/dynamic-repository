
package io.overcoded.repository.annotation.processor.generator;

import io.overcoded.repository.annotation.processor.domain.RepositoryData;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

import java.io.StringWriter;

/**
 * @author Adam Belak
 */
public class ProjectionGenerator implements SourceCodeGenerator {
    private static final String PROJECTION_TEMPLATE_FILE = "templates/projection_template.vm";
    private static final String ENTITY_TYPE_TEMPLATE_VARIABLE = "entityType";
    private static final String REST_RESOURCE_ENABLED = "restResourceEnabled";
    private static final String REST_RESOURCE_PATH = "restResourcePath";
    private static final String FETCH_SIZE_TEMPLATE_VARIABLE = "fetchSize";
    private static final String PACKAGE_NAME_TEMPLATE_VARIABLE = "packageName";
    private static final String SIMPLE_CLASS_NAME_TEMPLATE_VARIABLE = "simpleClassName";
    private static final String METHODS_TEMPLATE_VARIABLE = "methods";
    private static final String PACKAGE_SEPARATOR = ".";
    private static final String PROJECTION_SUFFIX = "Projection";

    @Override
    public SourceFile generate(RepositoryData repositoryData) {
        String simpleClassName = repositoryData.getEntityType() + PROJECTION_SUFFIX;
        String builderClassName = repositoryData.getPackageName() + PACKAGE_SEPARATOR + simpleClassName;
        repositoryData.setSimpleClassName(simpleClassName);

        boolean needsProjection = !repositoryData.getProjectedFields().isEmpty();
        return needsProjection
                ? SourceFile.builder()
                .filename(builderClassName)
                .content(generateContent(repositoryData))
                .build()
                : null;
    }

    private String generateContent(RepositoryData repositoryData) {
        VelocityEngine velocityEngine = createEngine();
        Template template = velocityEngine.getTemplate(PROJECTION_TEMPLATE_FILE);
        VelocityContext context = createContextForRepository(repositoryData);
        StringWriter writer = new StringWriter();
        template.merge(context, writer);
        return writer.toString();
    }

    private VelocityEngine createEngine() {
        VelocityEngine velocityEngine = new VelocityEngine();
        velocityEngine.setProperty(RuntimeConstants.RESOURCE_LOADERS, "classpath");
        velocityEngine.setProperty("resource.loader.classpath.class", ClasspathResourceLoader.class.getName());
        velocityEngine.init();
        return velocityEngine;
    }

    private VelocityContext createContextForRepository(RepositoryData repositoryData) {
        VelocityContext context = new VelocityContext();
        context.put(FETCH_SIZE_TEMPLATE_VARIABLE, repositoryData.getFetchSize());
        context.put(REST_RESOURCE_ENABLED, repositoryData.isRestResourceEnabled());
        context.put(REST_RESOURCE_PATH, repositoryData.getResourcePath());
        context.put(ENTITY_TYPE_TEMPLATE_VARIABLE, repositoryData.getEntityType());
        context.put(PACKAGE_NAME_TEMPLATE_VARIABLE, repositoryData.getPackageName());
        context.put(SIMPLE_CLASS_NAME_TEMPLATE_VARIABLE, repositoryData.getSimpleClassName());
        context.put(METHODS_TEMPLATE_VARIABLE, repositoryData.getProjectedFields());
        return context;
    }
}

package io.overcoded.repository.annotation.processor.generator;

import io.overcoded.repository.annotation.processor.domain.MethodData;
import io.overcoded.repository.annotation.processor.domain.RepositoryData;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

import java.io.StringWriter;
import java.util.Objects;

/**
 * @author Diana Ladanyi
 */
public class RepositoryGenerator implements SourceCodeGenerator {
    private static final String REPOSITORY_TEMPLATE_FILE = "templates/repository_template.vm";
    private static final String COUNT_METHOD_TEMPLATE_FILE = "templates/count_method_template.vm";
    private static final String FIND_METHOD_TEMPLATE_FILE = "templates/find_method_template.vm";
    private static final String FIND_ONE_METHOD_TEMPLATE_FILE = "templates/find_one_method_template.vm";
    private static final String ENTITY_TYPE_TEMPLATE_VARIABLE = "entityType";
    private static final String REST_RESOURCE_ENABLED = "restResourceEnabled";
    private static final String REST_RESOURCE_PATH = "restResourcePath";
    private static final String FETCH_SIZE_TEMPLATE_VARIABLE = "fetchSize";
    private static final String PACKAGE_NAME_TEMPLATE_VARIABLE = "packageName";
    private static final String SIMPLE_CLASS_NAME_TEMPLATE_VARIABLE = "simpleClassName";
    private static final String METHODS_TEMPLATE_VARIABLE = "methods";
    private static final String METHOD_NAME_TEMPLATE_VARIABLE = "methodName";
    private static final String PARAMETERS_TEMPLATE_VARIABLE = "parameters";
    private static final String REPOSITORY_CLASS_SUFFIX = "JpaRepository";
    private static final String PACKAGE_SEPARATOR = ".";

    @Override
    public SourceFile generate(RepositoryData repositoryData) {
        String suffix = getSafeSuffix(repositoryData);
        String simpleClassName = repositoryData.getEntityType() + suffix;
        String builderClassName = repositoryData.getPackageName() + PACKAGE_SEPARATOR + simpleClassName;
        repositoryData.setSimpleClassName(simpleClassName);

        return SourceFile.builder()
                .filename(builderClassName)
                .content(generateContent(repositoryData))
                .build();
    }

    private String generateContent(RepositoryData repositoryData) {
        VelocityEngine velocityEngine = createEngine();
        Template template = velocityEngine.getTemplate(REPOSITORY_TEMPLATE_FILE);
        VelocityContext context = createContextForRepository(repositoryData, velocityEngine);
        StringWriter writer = new StringWriter();
        template.merge(context, writer);
        return writer.toString();
    }

    private String generateMethods(VelocityEngine velocityEngine, RepositoryData repositoryData) {
        StringWriter writer = new StringWriter();
        createFindOneMethods(velocityEngine, repositoryData, writer);
        createFindMethods(velocityEngine, repositoryData, writer);
        createCountMethods(velocityEngine, repositoryData, writer);
        return writer.toString();
    }

    private void createFindOneMethods(VelocityEngine velocityEngine, RepositoryData repositoryData, StringWriter writer) {
        repositoryData.getFindOneMethods().forEach(methodData -> createMethod(velocityEngine, repositoryData, writer, methodData, FIND_ONE_METHOD_TEMPLATE_FILE));
    }

    private void createFindMethods(VelocityEngine velocityEngine, RepositoryData repositoryData, StringWriter writer) {
        repositoryData.getFindMethods().forEach(methodData -> createMethod(velocityEngine, repositoryData, writer, methodData, FIND_METHOD_TEMPLATE_FILE));
    }

    private void createCountMethods(VelocityEngine velocityEngine, RepositoryData repositoryData, StringWriter writer) {
        repositoryData.getCountMethods().forEach(methodData -> createMethod(velocityEngine, repositoryData, writer, methodData, COUNT_METHOD_TEMPLATE_FILE));
    }

    private void createMethod(VelocityEngine velocityEngine, RepositoryData repositoryData, StringWriter writer, MethodData methodData, String findMethodTemplateFile) {
        Template template = velocityEngine.getTemplate(findMethodTemplateFile);
        VelocityContext context = createContextForMethod(repositoryData, methodData);
        template.merge(context, writer);
    }

    private VelocityEngine createEngine() {
        VelocityEngine velocityEngine = new VelocityEngine();
        velocityEngine.setProperty(RuntimeConstants.RESOURCE_LOADERS, "classpath");
        velocityEngine.setProperty("resource.loader.classpath.class", ClasspathResourceLoader.class.getName());
        velocityEngine.init();
        return velocityEngine;
    }

    private VelocityContext createContextForRepository(RepositoryData repositoryData, VelocityEngine velocityEngine) {
        VelocityContext context = new VelocityContext();
        context.put(REST_RESOURCE_ENABLED, repositoryData.isRestResourceEnabled());
        context.put(REST_RESOURCE_PATH, repositoryData.getResourcePath());
        context.put(ENTITY_TYPE_TEMPLATE_VARIABLE, repositoryData.getEntityType());
        context.put(FETCH_SIZE_TEMPLATE_VARIABLE, repositoryData.getFetchSize());
        context.put(PACKAGE_NAME_TEMPLATE_VARIABLE, repositoryData.getPackageName());
        context.put(SIMPLE_CLASS_NAME_TEMPLATE_VARIABLE, repositoryData.getSimpleClassName());
        context.put(METHODS_TEMPLATE_VARIABLE, generateMethods(velocityEngine, repositoryData));
        return context;
    }

    private VelocityContext createContextForMethod(RepositoryData repositoryData, MethodData methodData) {
        VelocityContext context = new VelocityContext();
        context.put(REST_RESOURCE_ENABLED, repositoryData.isRestResourceEnabled());
        context.put(ENTITY_TYPE_TEMPLATE_VARIABLE, repositoryData.getEntityType());
        context.put(FETCH_SIZE_TEMPLATE_VARIABLE, repositoryData.getFetchSize());
        context.put(METHOD_NAME_TEMPLATE_VARIABLE, methodData.getMethodName());
        context.put(PARAMETERS_TEMPLATE_VARIABLE, methodData.getParameters());
        return context;
    }

    private String getSafeSuffix(RepositoryData repositoryData) {
        return Objects.nonNull(repositoryData.getSuffix()) && !repositoryData.getSuffix().isBlank() ? repositoryData.getSuffix().trim() : REPOSITORY_CLASS_SUFFIX;
    }
}

package io.overcoded.repository.annotation.processor.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.lang.model.element.Element;
import java.util.List;

/**
 * @author Diana Ladanyi
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ElementCollection {
    private Element dynamicRepositoryElement;
    private List<Element> findOneByElements;
    private List<Element> findAllByElements;
    private List<Element> projectedElements;
    private List<Element> findOneArrayElements;
    private List<Element> findAllArrayElements;
}

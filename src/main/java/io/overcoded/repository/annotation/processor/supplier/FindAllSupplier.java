package io.overcoded.repository.annotation.processor.supplier;

import io.overcoded.repository.annotation.FindAll;
import io.overcoded.repository.annotation.FindAllArray;
import io.overcoded.repository.annotation.processor.config.RepositoryProperties;
import io.overcoded.repository.annotation.processor.domain.ElementCollection;
import io.overcoded.repository.annotation.processor.domain.MethodData;
import io.overcoded.repository.annotation.processor.domain.RepositoryData;

import javax.lang.model.element.Element;
import javax.lang.model.type.MirroredTypeException;
import javax.lang.model.type.MirroredTypesException;
import javax.lang.model.type.TypeMirror;
import java.util.*;

/**
 * @author Diana Ladanyi
 * @author Adam Belak
 */
class FindAllSupplier extends AbstractSupplier implements Supplier {
    public static final String PARAMETER_SEPARATOR = ",";
    public static final String COUNT_METHOD_PREFIX = "count";
    private static final String METHOD_PARAMETER_SUFFIX = " arg";

    public FindAllSupplier(RepositoryProperties repositoryConfiguration) {
        super(repositoryConfiguration);
    }

    @Override
    public RepositoryData execute(ElementCollection elementCollection, RepositoryData repositoryData) {
        List<Element> findAllArrayElements = elementCollection.getFindAllArrayElements();
        if (Objects.nonNull(findAllArrayElements) && !findAllArrayElements.isEmpty()) {
            findAllArrayElements.forEach(findAllElement -> extractFindAllAnnotation(repositoryData, findAllElement));
        }
        return super.execute(elementCollection, repositoryData);
    }

    private void extractFindAllAnnotation(RepositoryData repositoryData, Element findAllElement) {
        FindAllArray findAllArray = findAllElement.getAnnotation(FindAllArray.class);
        if (Objects.nonNull(findAllArray) && findAllArray.value().length != 0) {
            Arrays.asList(findAllArray.value()).forEach(findAll -> createMethods(repositoryData, findAll));
        }

        FindAll findAll = findAllElement.getAnnotation(FindAll.class);
        if (Objects.nonNull(findAll)) {
            createMethods(repositoryData, findAll);
        }

    }

    private void createMethods(RepositoryData repositoryData, FindAll findAll) {
        String parameters = createParameters(findAll);
        String findAllMethod = getFindAllMethod(findAll);
        String countAllMethod = getCountAllMethod(findAllMethod);
        repositoryData.getFindMethods().add(MethodData.builder().methodName(findAllMethod).parameters(parameters).build());
        repositoryData.getCountMethods().add(MethodData.builder().methodName(countAllMethod).parameters(parameters).build());
    }

    private String createParameters(FindAll findAll) {
        List<String> parameterTypes = getParameterTypes(findAll);
        StringBuilder parameters = new StringBuilder();
        for (int i = 0; i < parameterTypes.size(); i++) {
            parameters.append(parameterTypes.get(i))
                    .append(METHOD_PARAMETER_SUFFIX)
                    .append(i);
            if (i != parameterTypes.size() - 1) {
                parameters.append(PARAMETER_SEPARATOR);
            }
        }
        return parameters.toString();
    }

    private String getFindAllMethod(FindAll findAll) {
        String method;
        try {
            method = findAll.method();
        } catch (MirroredTypeException e) {
            TypeMirror typeMirror = e.getTypeMirror();
            method = typeMirror.toString();
        }
        return method.replace("find", "");
    }

    private String getCountAllMethod(String findAllName) {
        return COUNT_METHOD_PREFIX + findAllName;
    }

    private List<String> getParameterTypes(FindAll findAll) {
        List<String> result = new ArrayList<>();
        try {
            Arrays.asList(findAll.types()).forEach(type -> result.add(type.getName()));
        } catch (MirroredTypesException e) {
            List<? extends TypeMirror> typeMirrors = e.getTypeMirrors();
            typeMirrors.forEach(typeMirror -> result.add(typeMirror.toString()));
        }
        return result;
    }
}

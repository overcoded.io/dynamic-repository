package io.overcoded.repository.annotation.processor.config;

import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Optional;
import java.util.Properties;

@Slf4j
public class RepositoryPropertiesReader {
    private static final String REPOSITORY_PROPERTIES = "repository.properties";
    private static final String SETTER_PREFIX = "set";

    public static RepositoryProperties read() {
        RepositoryProperties repositoryProperties = new RepositoryProperties();
        Properties properties = readProperties();
        properties.stringPropertyNames().forEach(property -> {
            getSetterMethod(property).ifPresent(method -> invokeMethod(repositoryProperties, properties, property, method));
        });
        return repositoryProperties;
    }

    private static void invokeMethod(RepositoryProperties repositoryProperties, Properties properties, String property, Method method) {
        Object value = properties.get(property);
        try {
            method.invoke(repositoryProperties, value);
        } catch (IllegalAccessException | InvocationTargetException e) {
            log.error("Failed to set {} to {}", property, value);
        }
    }

    private static Properties readProperties() {
        Properties properties = new Properties();
        try {
            properties.load(RepositoryPropertiesReader.class.getClassLoader().getResourceAsStream(REPOSITORY_PROPERTIES));
        } catch (Exception e) {
            log.warn("Failed to load repository.properties");
        }
        return properties;
    }

    private static Optional<Method> getSetterMethod(String property) {
        Optional<Method> method = Optional.empty();
        try {
            String setterMethodName = getSetterMethodName(property);
            method = Arrays.stream(RepositoryProperties.class.getDeclaredMethods())
                    .filter(m -> setterMethodName.equalsIgnoreCase(m.getName()))
                    .findFirst();
        } catch (Exception ex) {
            log.error("Failed to found setter method for {}", property);
        }
        return method;
    }

    private static String getSetterMethodName(String property) {
        return SETTER_PREFIX + property;
    }
}
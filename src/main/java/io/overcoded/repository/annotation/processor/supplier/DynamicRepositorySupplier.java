package io.overcoded.repository.annotation.processor.supplier;

import io.overcoded.repository.annotation.DynamicRepository;
import io.overcoded.repository.annotation.processor.config.RepositoryProperties;
import io.overcoded.repository.annotation.processor.domain.ElementCollection;
import io.overcoded.repository.annotation.processor.domain.RepositoryData;
import lombok.extern.slf4j.Slf4j;

import java.util.HashSet;
import java.util.Objects;

/**
 * @author Diana Ladanyi
 * @author Adam Belak
 */
@Slf4j
class DynamicRepositorySupplier extends AbstractSupplier implements Supplier {
    public DynamicRepositorySupplier(RepositoryProperties repositoryConfiguration) {
        super(repositoryConfiguration);
    }

    @Override
    public RepositoryData execute(ElementCollection elementCollection, RepositoryData repositoryData) {
        repositoryData.setFindMethods(new HashSet<>());
        repositoryData.setCountMethods(new HashSet<>());
        repositoryData.setFindOneMethods(new HashSet<>());
        repositoryData.setProjectedFields(new HashSet<>());
        repositoryData.setSuffix(getSuffix(elementCollection));
        repositoryData.setResourcePath(getResourcePath(elementCollection));
        repositoryData.setRestResourceEnabled(isRestResourceEnabled(elementCollection));
        repositoryData.setFetchSize(getFetchSize(elementCollection));
        repositoryData.setEntityType(elementCollection.getDynamicRepositoryElement().getSimpleName().toString());
        repositoryData.setPackageName(elementCollection.getDynamicRepositoryElement().getEnclosingElement().toString());
        return super.execute(elementCollection, repositoryData);
    }

    private boolean isRestResourceEnabled(ElementCollection elementCollection) {
        boolean restResourceEnabled = elementCollection.getDynamicRepositoryElement().getAnnotation(DynamicRepository.class).restResourceEnabled();
        if (!restResourceEnabled) {
            restResourceEnabled = Boolean.parseBoolean(repositoryConfiguration.getRestResourceEnabled());
        }
        return restResourceEnabled;
    }

    private String getResourcePath(ElementCollection elementCollection) {
        String resourcePath = elementCollection.getDynamicRepositoryElement().getAnnotation(DynamicRepository.class).resourcePath();
        return getStringProperty(resourcePath, repositoryConfiguration.getResourcePath());
    }

    private String getSuffix(ElementCollection elementCollection) {
        String suffix = elementCollection.getDynamicRepositoryElement().getAnnotation(DynamicRepository.class).suffix();
        return getStringProperty(suffix, repositoryConfiguration.getSuffix());
    }

    private String getFetchSize(ElementCollection elementCollection) {
        String fetchSize = elementCollection.getDynamicRepositoryElement().getAnnotation(DynamicRepository.class).streamFetchSize();
        return getStringProperty(fetchSize, repositoryConfiguration.getStreamFetchSize());
    }

    private String getStringProperty(String annotationValue, String commonValue) {
        if (Objects.isNull(annotationValue) || annotationValue.isBlank()) {
            annotationValue = commonValue;
        }
        return annotationValue;
    }
}

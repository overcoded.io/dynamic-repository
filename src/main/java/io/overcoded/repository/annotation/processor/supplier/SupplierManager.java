package io.overcoded.repository.annotation.processor.supplier;

import io.overcoded.repository.annotation.processor.domain.ElementCollection;
import io.overcoded.repository.annotation.processor.domain.RepositoryData;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;

/**
 * @author Diana Ladanyi
 * @author Adam Belak
 */
@RequiredArgsConstructor(access = AccessLevel.PACKAGE)
public class SupplierManager {
    private final SupplierChain supplierChain;

    public RepositoryData requestSupply(ElementCollection elementCollection, RepositoryData repositoryData) {
        return supplierChain.execute(elementCollection, repositoryData);
    }
}

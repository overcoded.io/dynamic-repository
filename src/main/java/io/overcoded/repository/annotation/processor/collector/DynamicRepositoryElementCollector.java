package io.overcoded.repository.annotation.processor.collector;

import io.overcoded.repository.annotation.processor.domain.ElementCollection;

import javax.lang.model.element.Element;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

/**
 * @author Diana Ladanyi
 * @author Adam Belak
 */
public class DynamicRepositoryElementCollector extends AbstractElementCollector implements ElementCollector {
    @Override
    public void accept(Map<String, ElementCollection> elementCollectionMap, Set<? extends Element> elements) {
        elements.forEach(element -> {
            String key = getKey(element);
            createNewElementCollectionIfNotPresent(elementCollectionMap, key);
            elementCollectionMap.get(key).setDynamicRepositoryElement(element);
        });
    }

    @Override
    protected Function<ElementCollection, List<Element>> extract() {
        return elementCollection -> List.of();
    }

    @Override
    protected String getKey(Element element) {
        return element.getSimpleName().toString();
    }
}

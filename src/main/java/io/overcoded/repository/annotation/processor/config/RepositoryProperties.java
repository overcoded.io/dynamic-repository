package io.overcoded.repository.annotation.processor.config;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RepositoryProperties {
    @Builder.Default
    private String streamFetchSize = "" + Integer.MIN_VALUE;

    @Builder.Default
    private String restResourceEnabled = "false";

    @Builder.Default
    private String suffix = "JpaRepository";

    @Builder.Default
    private String resourcePath = "";
}

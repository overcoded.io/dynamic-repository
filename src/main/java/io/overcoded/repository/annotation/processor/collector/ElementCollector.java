package io.overcoded.repository.annotation.processor.collector;

import io.overcoded.repository.annotation.processor.domain.ElementCollection;

import javax.lang.model.element.Element;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;

/**
 * @author Adam Belak
 */
public interface ElementCollector extends BiConsumer<Map<String, ElementCollection>, Set<? extends Element>> {
}

package io.overcoded.repository.annotation.processor.supplier;

import io.overcoded.repository.annotation.Projected;
import io.overcoded.repository.annotation.processor.config.RepositoryProperties;
import io.overcoded.repository.annotation.processor.domain.ElementCollection;
import io.overcoded.repository.annotation.processor.domain.MethodData;
import io.overcoded.repository.annotation.processor.domain.RepositoryData;

import javax.lang.model.element.Element;
import java.util.Objects;

import static javax.lang.model.type.TypeKind.BOOLEAN;

/**
 * @author Adam Belak
 */
class ProjectionSupplier extends AbstractSupplier implements Supplier {
    private static final String PROJECTION_NORMAL_GETTER_PREFIX = "get";
    private static final String PROJECTION_BOOLEAN_GETTER_PREFIX = "is";

    public ProjectionSupplier(RepositoryProperties repositoryConfiguration) {
        super(repositoryConfiguration);
    }

    @Override
    public RepositoryData execute(ElementCollection elementCollection, RepositoryData repositoryData) {
        if (Objects.nonNull(elementCollection.getProjectedElements()) && !elementCollection.getProjectedElements().isEmpty()) {
            elementCollection.getProjectedElements()
                    .forEach(projectionElement -> addProjectionAnnotation(repositoryData, projectionElement));
        }
        return super.execute(elementCollection, repositoryData);
    }

    private void addProjectionAnnotation(RepositoryData repositoryData, Element projectedElement) {
        repositoryData.getProjectedFields().add(createMethodData(projectedElement));
    }

    private MethodData createMethodData(Element projectedElement) {
        boolean isBooleanMethod = projectedElement.asType().getKind() == BOOLEAN && projectedElement.asType().getKind().isPrimitive();
        String prefix = isBooleanMethod ? PROJECTION_BOOLEAN_GETTER_PREFIX : PROJECTION_NORMAL_GETTER_PREFIX;
        return MethodData
                .builder()
                .methodName(createMethod(prefix, projectedElement))
                .parameters(projectedElement.asType().toString())
                .build();
    }

    private String createMethod(String prefix, Element projectedElement) {
        String elementName = projectedElement.getSimpleName().toString();
        Projected projected = projectedElement.getAnnotation(Projected.class);
        return prefix + elementName.substring(0, 1).toUpperCase() + elementName.substring(1);
    }
}

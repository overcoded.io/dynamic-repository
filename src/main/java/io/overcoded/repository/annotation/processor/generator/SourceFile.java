package io.overcoded.repository.annotation.processor.generator;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SourceFile {
    private String filename;
    private String content;
}

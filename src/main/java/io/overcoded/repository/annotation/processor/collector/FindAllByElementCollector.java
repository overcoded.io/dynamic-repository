package io.overcoded.repository.annotation.processor.collector;

import io.overcoded.repository.annotation.processor.domain.ElementCollection;

import javax.lang.model.element.Element;
import java.util.List;
import java.util.function.Function;

/**
 * @author Diana Ladanyi
 * @author Adam Belak
 */
public class FindAllByElementCollector extends AbstractElementCollector implements ElementCollector {
    @Override
    protected Function<ElementCollection, List<Element>> extract() {
        return ElementCollection::getFindAllByElements;
    }

    @Override
    protected String getKey(Element element) {
        return element.getEnclosingElement().getSimpleName().toString();
    }
}

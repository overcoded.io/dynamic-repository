package io.overcoded.repository.annotation.processor.supplier;

import io.overcoded.repository.annotation.FindOneBy;
import io.overcoded.repository.annotation.processor.config.RepositoryProperties;
import io.overcoded.repository.annotation.processor.domain.ElementCollection;
import io.overcoded.repository.annotation.processor.domain.MethodData;
import io.overcoded.repository.annotation.processor.domain.RepositoryData;

import javax.lang.model.element.Element;
import java.util.Objects;

/**
 * @author Adam Belak
 */
class FindOneBySupplier extends AbstractSupplier implements Supplier {
    private static final String FIND_ONE_BY_METHOD_PREFIX = "OneBy";
    private static final String METHOD_PARAMETER_SUFFIX = " arg0";

    public FindOneBySupplier(RepositoryProperties repositoryConfiguration) {
        super(repositoryConfiguration);
    }

    @Override
    public RepositoryData execute(ElementCollection elementCollection, RepositoryData repositoryData) {
        if (Objects.nonNull(elementCollection.getFindOneByElements()) && !elementCollection.getFindOneByElements().isEmpty()) {
            elementCollection.getFindOneByElements()
                    .forEach(findOneByElement -> addFindOneByAnnotation(repositoryData, findOneByElement));
        }
        return super.execute(elementCollection, repositoryData);
    }

    private void addFindOneByAnnotation(RepositoryData repositoryData, Element findOneByElement) {
        repositoryData.getFindOneMethods().add(createMethodData(findOneByElement));
    }

    private MethodData createMethodData(Element findOneByElement) {
        return MethodData
                .builder()
                .methodName(createMethod(FIND_ONE_BY_METHOD_PREFIX, findOneByElement))
                .parameters(getTypeName(findOneByElement) + METHOD_PARAMETER_SUFFIX)
                .build();
    }

    private String createMethod(String prefix, Element findOneByElement) {
        String elementName = findOneByElement.getSimpleName().toString();
        FindOneBy findOneBy = findOneByElement.getAnnotation(FindOneBy.class);
        String modifier = Objects.nonNull(findOneBy) ? findOneBy.modifier() : "";
        return prefix + elementName.substring(0, 1).toUpperCase() + elementName.substring(1) + modifier;
    }

    private String getTypeName(Element findAllByElement) {
        String typeName = findAllByElement
                .asType()
                .toString()
                .replace("java.lang.", "");
        if (typeName.contains("@") && typeName.contains(" ")) {
            typeName = typeName.substring(typeName.lastIndexOf(" ")).trim();
        }
        return typeName;
    }
}

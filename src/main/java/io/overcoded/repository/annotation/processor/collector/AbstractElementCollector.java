package io.overcoded.repository.annotation.processor.collector;

import io.overcoded.repository.annotation.processor.domain.ElementCollection;

import javax.lang.model.element.Element;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

/**
 * @author Adam Belak
 */
public abstract class AbstractElementCollector implements ElementCollector {
    @Override
    public void accept(Map<String, ElementCollection> elementCollectionMap, Set<? extends Element> elements) {
        elements.forEach(element -> {
            String key = getKey(element);
            createNewElementCollectionIfNotPresent(elementCollectionMap, key);
            extract().apply(elementCollectionMap.get(key)).add(element);
        });
    }

    protected abstract Function<ElementCollection, List<Element>> extract();

    protected abstract String getKey(Element element);

    protected void createNewElementCollectionIfNotPresent(Map<String, ElementCollection> elementCollectionMap, String key) {
        if (!elementCollectionMap.containsKey(key)) {
            elementCollectionMap.put(key, createElementCollection());
        }
    }

    private ElementCollection createElementCollection() {
        return ElementCollection
                .builder()
                .findAllArrayElements(new ArrayList<>())
                .findAllByElements(new ArrayList<>())
                .findOneArrayElements(new ArrayList<>())
                .findOneByElements(new ArrayList<>())
                .projectedElements(new ArrayList<>())
                .build();
    }
}

package io.overcoded.repository.annotation.processor.generator;

import io.overcoded.repository.annotation.processor.domain.RepositoryData;

import javax.annotation.processing.Filer;
import javax.tools.JavaFileObject;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Objects;

public class SourceCodeWriter {
    private final List<SourceCodeGenerator> generators = List.of(new RepositoryGenerator(), new ProjectionGenerator());

    public void generate(RepositoryData repositoryData, Filer filer) {
        generators.stream()
                .map(generator -> generator.generate(repositoryData.toBuilder().build()))
                .filter(Objects::nonNull)
                .forEach(sourceFile -> writeJavaFile(sourceFile, filer));
    }

    private void writeJavaFile(SourceFile sourceFile, Filer filer) {
        try {
            JavaFileObject builderFile = filer.createSourceFile(sourceFile.getFilename());
            try (PrintWriter out = new PrintWriter(builderFile.openWriter())) {
                out.println(sourceFile.getContent());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

package io.overcoded.repository.annotation.processor.supplier;

import io.overcoded.repository.annotation.FindOne;
import io.overcoded.repository.annotation.FindOneArray;
import io.overcoded.repository.annotation.processor.config.RepositoryProperties;
import io.overcoded.repository.annotation.processor.domain.ElementCollection;
import io.overcoded.repository.annotation.processor.domain.MethodData;
import io.overcoded.repository.annotation.processor.domain.RepositoryData;

import javax.lang.model.element.Element;
import javax.lang.model.type.MirroredTypeException;
import javax.lang.model.type.MirroredTypesException;
import javax.lang.model.type.TypeMirror;
import java.util.*;

/**
 * @author Diana Ladanyi
 * @author Adam Belak
 */
class FindOneSupplier extends AbstractSupplier implements Supplier {
    public static final String PARAMETER_SEPARATOR = ",";
    private static final String METHOD_PARAMETER_SUFFIX = " arg";

    public FindOneSupplier(RepositoryProperties repositoryConfiguration) {
        super(repositoryConfiguration);
    }

    @Override
    public RepositoryData execute(ElementCollection elementCollection, RepositoryData repositoryData) {
        List<Element> findOneArrayElements = elementCollection.getFindOneArrayElements();
        if (Objects.nonNull(findOneArrayElements) && !findOneArrayElements.isEmpty()) {
            findOneArrayElements.forEach(findAllElement -> extractFindOneAnnotation(repositoryData, findAllElement));
        }
        return super.execute(elementCollection, repositoryData);
    }

    private void extractFindOneAnnotation(RepositoryData repositoryData, Element findOneElement) {
        FindOneArray findOneArray = findOneElement.getAnnotation(FindOneArray.class);
        if (Objects.nonNull(findOneArray) && findOneArray.value().length != 0) {
            Arrays.asList(findOneArray.value()).forEach(findAll -> createMethod(repositoryData, findAll));
        }
        FindOne findOne = findOneElement.getAnnotation(FindOne.class);
        if (Objects.nonNull(findOne)) {
            createMethod(repositoryData, findOne);
        }

    }

    private void createMethod(RepositoryData repositoryData, FindOne findOne) {
        String parameters = createParameters(findOne);
        String findOneMethod = getFindOneMethod(findOne);
        if (Objects.nonNull(findOneMethod) && findOneMethod.startsWith("find")) {
            findOneMethod = findOneMethod.substring(4);
        }
        repositoryData.getFindOneMethods().add(MethodData.builder().methodName(findOneMethod).parameters(parameters).build());
    }

    private String createParameters(FindOne findOne) {
        List<String> parameterTypes = getParameterTypes(findOne);
        StringBuilder parameters = new StringBuilder();
        for (int i = 0; i < parameterTypes.size(); i++) {
            parameters.append(parameterTypes.get(i))
                    .append(METHOD_PARAMETER_SUFFIX)
                    .append(i);
            if (i != parameterTypes.size() - 1) {
                parameters.append(PARAMETER_SEPARATOR);
            }
        }
        return parameters.toString();
    }

    private String getFindOneMethod(FindOne findOne) {
        String method;
        try {
            method = findOne.method();
        } catch (MirroredTypeException e) {
            TypeMirror typeMirror = e.getTypeMirror();
            method = typeMirror.toString();
        }
        return method;
    }


    private List<String> getParameterTypes(FindOne findOne) {
        List<String> result = new ArrayList<>();
        try {
            Arrays.asList(findOne.types()).forEach(type -> result.add(type.getName()));
        } catch (MirroredTypesException e) {
            List<? extends TypeMirror> typeMirrors = e.getTypeMirrors();
            typeMirrors.forEach(typeMirror -> result.add(typeMirror.toString()));
        }
        return result;
    }
}

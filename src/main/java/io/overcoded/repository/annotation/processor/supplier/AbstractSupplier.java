package io.overcoded.repository.annotation.processor.supplier;

import io.overcoded.repository.annotation.processor.config.RepositoryProperties;
import io.overcoded.repository.annotation.processor.domain.ElementCollection;
import io.overcoded.repository.annotation.processor.domain.RepositoryData;
import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

/**
 * @author Diana Ladanyi
 */
@Getter
@Setter
abstract class AbstractSupplier implements Supplier {
    private Supplier next;
    protected RepositoryProperties repositoryConfiguration;

    public AbstractSupplier(RepositoryProperties repositoryConfiguration) {
        this.repositoryConfiguration = repositoryConfiguration;
    }

    @Override
    public Supplier getLast() {
        Supplier last = this;
        while (Objects.nonNull(last.getNext())) {
            last = last.getNext();
        }
        return last;
    }

    @Override
    public RepositoryData execute(ElementCollection elementCollection, RepositoryData repositoryData) {
        return Objects.nonNull(getNext())
                ? getNext().execute(elementCollection, repositoryData)
                : repositoryData;
    }
}

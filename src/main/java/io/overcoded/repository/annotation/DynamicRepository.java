package io.overcoded.repository.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The outcome is a generated public JpaRepository in the same package with the domain class compile time.
 * Only with @Entity annotated classes.
 *
 * @author Diana Ladanyi
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.SOURCE)
public @interface DynamicRepository {
    /**
     * Defines suffix of repository interface. The default value is JpaRepository.
     * <p>
     * If you want to change the suffix for all of your repositories,
     * you can do it in your `repository.properties`.
     *
     * @return string
     */
    String suffix() default "";

    /**
     * If you want to expose jpa repositories as rest endpoints,enable this option.
     * <p>
     * Spring Boot default expose jpa repositories if spring data rest is on classpath.
     * For now, we can't detect the classpath, so you have to enable/disable manually.
     * <p>
     * False means we won't generate spring data rest related entities into the source
     * code of the generated repository.
     * <p>
     * If you want to change the suffix for all of your repositories,
     * you can do it in your `repository.properties`.
     *
     * @return boolean
     */
    boolean restResourceEnabled() default false;

    /**
     * If you want to rename the endpoint, just change this
     * <p>
     * If you want to change the suffix for all of your repositories,
     * you can do it in your `repository.properties`.
     *
     * @return ""
     */
    String resourcePath() default "";

    /**
     * You can configure fetch size for stream based methods
     * <p>
     * If you want to change the suffix for all of your repositories,
     * you can do it in your `repository.properties`.
     *
     * @return
     */
    String streamFetchSize() default "";
}

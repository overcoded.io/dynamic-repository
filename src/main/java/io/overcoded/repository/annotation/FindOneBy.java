package io.overcoded.repository.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The outcome is one generated method in the public JpaRepository of the domain class compile time.
 * 'findOneBy{fieldName}({FieldType} {fieldName})'.
 * Only with @DynamicRepository annotated classes.
 *
 * @author Adam Belak
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.SOURCE)
public @interface FindOneBy {
    /**
     * defines suffix for findOne method
     *
     * @return string
     */
    String modifier() default "";
}

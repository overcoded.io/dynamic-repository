package io.overcoded.repository.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * A helper class to allow repeatable annotations.
 *
 * @author Diana Ladanyi
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.SOURCE)
public @interface FindAllArray {
    FindAll[] value();
}

package io.overcoded.repository.annotation;

import java.lang.annotation.*;

/**
 * The outcome is two generated methods in the public JpaRepository of the domain class compile time.
 * 'findAllBy{methodName}({types...}, Pageable pageable)' and 'countAllBy{methodName}({types...})'
 * Only with @DynamicRepository annotated classes.
 *
 * @author Diana Ladanyi
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.SOURCE)
@Repeatable(value = FindAllArray.class)
public @interface FindAll {
    /**
     * defines suffix for findAll/countAll methods
     *
     * @return string
     */
    String method() default "";

    /**
     * defines method argument types
     *
     * @return Class array
     */
    Class<?>[] types() default {};

}

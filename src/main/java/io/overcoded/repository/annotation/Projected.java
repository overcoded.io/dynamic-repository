package io.overcoded.repository.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The outcome is one generated interface, with getter methods of annotated fields and an extra method
 * in the repository to support projections.
 * Only works with @DynamicRepository annotated classes.
 *
 * @author Adam Belak
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.SOURCE)
public @interface Projected {
}

package io.overcoded.repository.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The outcome is two generated methods in the public JpaRepository of the domain class compile time.
 * 'findAllBy{fieldName}({FieldType} {fieldName}, Pageable pageable)' and 'countAllBy{fieldName}({FieldType} {fieldName})'
 * Only with @DynamicRepository annotated classes.
 *
 * @author Diana Ladanyi
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.SOURCE)
public @interface FindAllBy {
    /**
     * defines suffix for findAll/countAll methods
     *
     * @return string
     */
    String modifier() default "";
}

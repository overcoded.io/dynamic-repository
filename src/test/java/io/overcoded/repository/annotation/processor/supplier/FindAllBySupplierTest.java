package io.overcoded.repository.annotation.processor.supplier;

import io.overcoded.repository.annotation.FindAllBy;
import io.overcoded.repository.annotation.processor.config.RepositoryProperties;
import io.overcoded.repository.annotation.processor.domain.ElementCollection;
import io.overcoded.repository.annotation.processor.domain.MethodData;
import io.overcoded.repository.annotation.processor.domain.RepositoryData;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.lang.model.element.Element;
import javax.lang.model.element.Name;
import javax.lang.model.type.TypeMirror;
import java.lang.annotation.Annotation;
import java.util.HashSet;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class FindAllBySupplierTest {
    private FindAllBySupplier testSubject;

    @BeforeEach
    void setUp() {
        testSubject = new FindAllBySupplier(new RepositoryProperties());
    }

    @Test
    void executeShouldDoNothingWhenFindAllByElementsIsNull() {
        RepositoryData actual = new RepositoryData();
        testSubject.execute(ElementCollection.builder().build(), actual);

        assertThat(actual).isEqualTo(new RepositoryData());
    }

    @Test
    void executeShouldDoNothingWhenFindAllByElementsIsEmpty() {
        RepositoryData actual = new RepositoryData();
        testSubject.execute(ElementCollection.builder().findAllByElements(List.of()).build(), actual);

        assertThat(actual).isEqualTo(new RepositoryData());
    }

    @Test
    void executeShouldAddFindMethodAndCountMethodForEachFindAllByElementInElementCollection() {
        Element nameField = createMockElement("Name", "String", createFindAllBy(""));
        Element ageField = createMockElement("Age", "Long", createFindAllBy("LessThan"));

        RepositoryData actual = RepositoryData.builder().findMethods(new HashSet<>()).countMethods(new HashSet<>()).build();
        testSubject.execute(ElementCollection.builder().findAllByElements(List.of(nameField, ageField)).build(), actual);


        assertThat(actual.getFindMethods())
                .contains(
                        MethodData.builder().methodName("AllByName").parameters("String arg0").build(),
                        MethodData.builder().methodName("AllByAgeLessThan").parameters("Long arg0").build()
                );
        assertThat(actual.getCountMethods())
                .contains(
                        MethodData.builder().methodName("countAllByName").parameters("String arg0").build(),
                        MethodData.builder().methodName("countAllByAgeLessThan").parameters("Long arg0").build()
                );
    }

    private FindAllBy createFindAllBy(String modifier) {
        return new FindAllBy() {

            @Override
            public Class<? extends Annotation> annotationType() {
                return FindAllBy.class;
            }

            @Override
            public String modifier() {
                return modifier;
            }
        };
    }

    private Element createMockElement(String fieldName, String typeName, FindAllBy findAllBy) {
        Name name = mock(Name.class);
        when(name.toString()).thenReturn(fieldName);

        TypeMirror typeMirror = mock(TypeMirror.class);
        when(typeMirror.toString()).thenReturn(typeName);

        Element element = mock(Element.class);
        when(element.getSimpleName()).thenReturn(name);
        when(element.asType()).thenReturn(typeMirror);
        when(element.getAnnotation(FindAllBy.class)).thenReturn(findAllBy);
        return element;
    }
}
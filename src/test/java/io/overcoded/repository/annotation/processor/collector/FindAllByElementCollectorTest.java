package io.overcoded.repository.annotation.processor.collector;

import io.overcoded.repository.annotation.processor.domain.ElementCollection;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.lang.model.element.Element;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class FindAllByElementCollectorTest {
    private FindAllByElementCollector testSubject;
    private Element inputElement;
    private Name inputName;
    private Element enclosingElement;

    @BeforeEach
    void setUp() {
        testSubject = new FindAllByElementCollector();

        inputElement = mock(TypeElement.class);
        inputName = mock(Name.class);
        enclosingElement = mock(Element.class);

        when(inputName.toString()).thenReturn("SimpleElementName");
        when(inputElement.getEnclosingElement()).thenReturn(enclosingElement);
        when(enclosingElement.getSimpleName()).thenReturn(inputName);
    }

    @Test
    void acceptShouldCreateANewMapEntryIfElementIsNotPresentAndSetTheElementToIt() {
        Set<? extends Element> elements = Set.of(inputElement);
        Map<String, ElementCollection> elementCollectionMap = new HashMap<>();

        testSubject.accept(elementCollectionMap, elements);

        ElementCollection expected = ElementCollection.builder()
                .findAllByElements(List.of(inputElement))
                .findAllArrayElements(new ArrayList<>())
                .findOneByElements(new ArrayList<>())
                .findOneArrayElements(new ArrayList<>())
                .projectedElements(new ArrayList<>())
                .build();
        assertThat(elementCollectionMap).containsKeys("SimpleElementName").containsValue(expected);
    }

    @Test
    void acceptShouldUpdateAnExistingElementInTheMapWhenKeyIsPresent() {
        Set<? extends Element> elements = Set.of(inputElement);
        ArrayList<Element> findAllByElements = new ArrayList<>(List.of(mock(TypeElement.class)));
        ElementCollection originalCollection = ElementCollection.builder().findAllByElements(findAllByElements).build();
        Map<String, ElementCollection> elementCollectionMap = Map.of("SimpleElementName", originalCollection);

        testSubject.accept(elementCollectionMap, elements);

        originalCollection.setDynamicRepositoryElement(inputElement);
        assertThat(elementCollectionMap)
                .hasEntrySatisfying("SimpleElementName", elementCollection -> assertThat(elementCollection.getFindAllByElements())
                        .hasSize(2)
                        .contains(inputElement));
    }
}
package io.overcoded.repository.annotation.processor.supplier;

import io.overcoded.repository.annotation.FindAll;
import io.overcoded.repository.annotation.FindAllArray;
import io.overcoded.repository.annotation.processor.config.RepositoryProperties;
import io.overcoded.repository.annotation.processor.domain.ElementCollection;
import io.overcoded.repository.annotation.processor.domain.MethodData;
import io.overcoded.repository.annotation.processor.domain.RepositoryData;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.lang.model.element.Element;
import java.lang.annotation.Annotation;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class FindAllSupplierTest {
    private FindAllSupplier testSubject;

    @BeforeEach
    void setUp() {
        testSubject = new FindAllSupplier(new RepositoryProperties());
    }

    @Test
    void executeShouldDoNothingWhenFindAllArrayElementsIsNull() {
        RepositoryData actual = new RepositoryData();
        testSubject.execute(ElementCollection.builder().build(), actual);

        assertThat(actual).isEqualTo(new RepositoryData());
    }

    @Test
    void executeShouldDoNothingWhenFindAllArrayElementsIsEmpty() {
        RepositoryData actual = new RepositoryData();
        testSubject.execute(ElementCollection.builder().findAllArrayElements(List.of()).build(), actual);

        assertThat(actual).isEqualTo(new RepositoryData());
    }

    @Test
    void executeShouldDoNothingWhenFindAllArrayElementsIsNotEmptyButThereIsNoFindAllArrayOrFindAllAnnotationOnElement() {
        RepositoryData actual = new RepositoryData();
        testSubject.execute(ElementCollection.builder().findAllArrayElements(List.of(createMockElement(null, null))).build(), actual);

        assertThat(actual).isEqualTo(new RepositoryData());
    }

    @Test
    void executeShouldDoNothingWhenFindAllByElementsIsNotEmptyButThereIsNoFindAllAnnotationButThereIsAFindAllArrayAnnotationOnElementButItIsEmpty() {
        RepositoryData actual = new RepositoryData();
        FindAllArray findAllArrayResult = createFindAllArrayAnnotation(null);
        testSubject.execute(ElementCollection.builder().findAllByElements(List.of(createMockElement(findAllArrayResult, null))).build(), actual);

        assertThat(actual).isEqualTo(new RepositoryData());
    }

    @Test
    void executeShouldAddFindMethodAndCountMethodForFindAllAnnotationOnElementInElementCollection() {
        FindAll findAllResult = createFindAllAnnotation();
        RepositoryData actual = RepositoryData.builder().countMethods(new HashSet<>()).findMethods(new HashSet<>()).build();
        testSubject.execute(ElementCollection.builder().findAllArrayElements(List.of(createMockElement(null, findAllResult))).build(), actual);

        assertThat(actual.getFindMethods())
                .contains(
                        MethodData.builder().methodName("AllByNameEqualsIgnoreCaseAndAgeGreaterThan").parameters("java.lang.String arg0,java.lang.Integer arg1").build()
                );
        assertThat(actual.getCountMethods())
                .contains(
                        MethodData.builder().methodName("countAllByNameEqualsIgnoreCaseAndAgeGreaterThan").parameters("java.lang.String arg0,java.lang.Integer arg1").build()
                );
    }

    @Test
    void executeShouldAddFindMethodAndCountMethodForFindAllAnnotationInFindAllAnnotationOnElementInElementCollection() {
        FindAllArray findAllArrayAnnotation = createFindAllArrayAnnotation(createFindAllAnnotation());
        RepositoryData actual = RepositoryData.builder().countMethods(new HashSet<>()).findMethods(new HashSet<>()).build();
        testSubject.execute(ElementCollection.builder().findAllArrayElements(List.of(createMockElement(findAllArrayAnnotation, null))).build(), actual);

        assertThat(actual.getFindMethods())
                .contains(
                        MethodData.builder().methodName("AllByNameEqualsIgnoreCaseAndAgeGreaterThan").parameters("java.lang.String arg0,java.lang.Integer arg1").build()
                );
        assertThat(actual.getCountMethods())
                .contains(
                        MethodData.builder().methodName("countAllByNameEqualsIgnoreCaseAndAgeGreaterThan").parameters("java.lang.String arg0,java.lang.Integer arg1").build()
                );
    }

    private FindAllArray createFindAllArrayAnnotation(FindAll findAllAnnotation) {
        return new FindAllArray() {

            @Override
            public Class<? extends Annotation> annotationType() {
                return FindAllArray.class;
            }

            @Override
            public FindAll[] value() {
                return Objects.nonNull(findAllAnnotation)
                        ? new FindAll[]{findAllAnnotation}
                        : new FindAll[0];
            }
        };
    }

    private FindAll createFindAllAnnotation() {
        return new FindAll() {
            @Override
            public Class<? extends Annotation> annotationType() {
                return FindAll.class;
            }

            @Override
            public String method() {
                return "AllByNameEqualsIgnoreCaseAndAgeGreaterThan";
            }

            @Override
            public Class<?>[] types() {
                return new Class[]{String.class, Integer.class};
            }
        };
    }

    private Element createMockElement(FindAllArray findAllArrayResult, FindAll findAllResult) {
        Element element = mock(Element.class);
        when(element.getAnnotation(FindAllArray.class)).thenReturn(findAllArrayResult);
        when(element.getAnnotation(FindAll.class)).thenReturn(findAllResult);
        return element;
    }
}
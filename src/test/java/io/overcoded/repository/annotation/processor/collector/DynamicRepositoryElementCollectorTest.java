package io.overcoded.repository.annotation.processor.collector;

import io.overcoded.repository.annotation.processor.domain.ElementCollection;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.lang.model.element.Element;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class DynamicRepositoryElementCollectorTest {
    private DynamicRepositoryElementCollector testSubject;
    private Element inputElement;
    private Name inputName;

    @BeforeEach
    void setUp() {
        testSubject = new DynamicRepositoryElementCollector();

        inputElement = mock(TypeElement.class);
        inputName = mock(Name.class);

        when(inputName.toString()).thenReturn("SimpleElementName");
        when(inputElement.getSimpleName()).thenReturn(inputName);
    }

    @Test
    void acceptShouldCreateANewMapEntryIfElementIsNotPresentAndSetTheElementToIt() {
        Set<? extends Element> elements = Set.of(inputElement);
        Map<String, ElementCollection> elementCollectionMap = new HashMap<>();

        testSubject.accept(elementCollectionMap, elements);

        ElementCollection expected = ElementCollection.builder()
                .dynamicRepositoryElement(inputElement)
                .findAllByElements(new ArrayList<>())
                .findAllArrayElements(new ArrayList<>())
                .findOneByElements(new ArrayList<>())
                .findOneArrayElements(new ArrayList<>())
                .projectedElements(new ArrayList<>())
                .build();
        assertThat(elementCollectionMap).containsKeys("SimpleElementName").containsValue(expected);
    }

    @Test
    void acceptShouldUpdateAnExistingElementInTheMapWhenKeyIsPresent() {
        Set<? extends Element> elements = Set.of(inputElement);
        ElementCollection originalCollection = ElementCollection.builder().build();
        Map<String, ElementCollection> elementCollectionMap = Map.of("SimpleElementName", originalCollection);

        testSubject.accept(elementCollectionMap, elements);

        originalCollection.setDynamicRepositoryElement(inputElement);
        assertThat(elementCollectionMap)
                .hasEntrySatisfying("SimpleElementName", elementCollection -> assertThat(elementCollection.getDynamicRepositoryElement())
                        .isEqualTo(inputElement));
    }
}
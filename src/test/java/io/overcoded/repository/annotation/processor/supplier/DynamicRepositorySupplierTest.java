package io.overcoded.repository.annotation.processor.supplier;

import io.overcoded.repository.annotation.DynamicRepository;
import io.overcoded.repository.annotation.processor.config.RepositoryProperties;
import io.overcoded.repository.annotation.processor.domain.ElementCollection;
import io.overcoded.repository.annotation.processor.domain.RepositoryData;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.lang.model.element.Element;
import javax.lang.model.element.Name;
import java.util.HashSet;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class DynamicRepositorySupplierTest {
    private DynamicRepositorySupplier testSubject;

    @BeforeEach
    void setUp() {
        testSubject = new DynamicRepositorySupplier(new RepositoryProperties());
    }

    @Test
    void executeShouldInitiatePackageNameTypeNameFindAndCountMethodsOfRepositoryData() {
        Name simpleName = mock(Name.class);
        Element packageElement = mock(Element.class);
        Element dynamicRepositoryElement = mock(Element.class);
        ElementCollection elementCollection = ElementCollection
                .builder()
                .dynamicRepositoryElement(dynamicRepositoryElement)
                .build();
        DynamicRepository dynamicRepository = mock(DynamicRepository.class);

        when(dynamicRepository.suffix()).thenReturn("Repository");
        when(dynamicRepositoryElement.getAnnotation(DynamicRepository.class)).thenReturn(dynamicRepository);
        when(dynamicRepositoryElement.getSimpleName()).thenReturn(simpleName);
        when(dynamicRepositoryElement.getEnclosingElement()).thenReturn(packageElement);
        when(simpleName.toString()).thenReturn("SimpleTypeName");
        when(packageElement.toString()).thenReturn("com.example.test");

        RepositoryData repositoryData = new RepositoryData();
        testSubject.execute(elementCollection, repositoryData);

        assertThat(repositoryData)
                .hasFieldOrPropertyWithValue("entityType", "SimpleTypeName")
                .hasFieldOrPropertyWithValue("packageName", "com.example.test")
                .hasFieldOrPropertyWithValue("suffix", "Repository")
                .hasFieldOrPropertyWithValue("findMethods", new HashSet<>())
                .hasFieldOrPropertyWithValue("countMethods", new HashSet<>());
    }
}
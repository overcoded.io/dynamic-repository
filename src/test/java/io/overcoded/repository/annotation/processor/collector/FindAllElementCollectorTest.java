package io.overcoded.repository.annotation.processor.collector;

import io.overcoded.repository.annotation.processor.domain.ElementCollection;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.lang.model.element.Element;
import javax.lang.model.element.Name;
import javax.lang.model.element.TypeElement;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class FindAllElementCollectorTest {
    private FindAllElementCollector testSubject;
    private Element inputElement;
    private Name inputName;

    @BeforeEach
    void setUp() {
        testSubject = new FindAllElementCollector();

        inputElement = mock(TypeElement.class);
        inputName = mock(Name.class);

        when(inputName.toString()).thenReturn("SimpleElementName");
        when(inputElement.getSimpleName()).thenReturn(inputName);
    }

    @Test
    void acceptShouldCreateANewMapEntryIfElementIsNotPresentAndSetTheElementToIt() {
        Set<? extends Element> elements = Set.of(inputElement);
        Map<String, ElementCollection> elementCollectionMap = new HashMap<>();

        testSubject.accept(elementCollectionMap, elements);

        ElementCollection expected = ElementCollection.builder()
                .findAllArrayElements(List.of(inputElement))
                .findAllByElements(new ArrayList<>())
                .findOneArrayElements(new ArrayList<>())
                .findOneByElements(new ArrayList<>())
                .projectedElements(new ArrayList<>())
                .build();
        assertThat(elementCollectionMap).containsKeys("SimpleElementName").containsValue(expected);
    }

    @Test
    void acceptShouldUpdateAnExistingElementInTheMapWhenKeyIsPresent() {
        Set<? extends Element> elements = Set.of(inputElement);
        ArrayList<Element> findAllElements = new ArrayList<>(List.of(mock(TypeElement.class)));
        ElementCollection originalCollection = ElementCollection.builder().findAllArrayElements(findAllElements).build();
        Map<String, ElementCollection> elementCollectionMap = Map.of("SimpleElementName", originalCollection);

        testSubject.accept(elementCollectionMap, elements);

        originalCollection.setDynamicRepositoryElement(inputElement);
        assertThat(elementCollectionMap)
                .hasEntrySatisfying("SimpleElementName", elementCollection -> assertThat(elementCollection.getFindAllArrayElements())
                        .hasSize(2)
                        .contains(inputElement));
    }
}
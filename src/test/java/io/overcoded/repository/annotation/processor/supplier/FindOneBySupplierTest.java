package io.overcoded.repository.annotation.processor.supplier;

import io.overcoded.repository.annotation.FindAllBy;
import io.overcoded.repository.annotation.FindOneBy;
import io.overcoded.repository.annotation.processor.config.RepositoryProperties;
import io.overcoded.repository.annotation.processor.domain.ElementCollection;
import io.overcoded.repository.annotation.processor.domain.MethodData;
import io.overcoded.repository.annotation.processor.domain.RepositoryData;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.lang.model.element.Element;
import javax.lang.model.element.Name;
import javax.lang.model.type.TypeMirror;
import java.lang.annotation.Annotation;
import java.util.HashSet;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class FindOneBySupplierTest {
    private FindOneBySupplier testSubject;

    @BeforeEach
    void setUp() {
        testSubject = new FindOneBySupplier(new RepositoryProperties());
    }

    @Test
    void executeShouldDoNothingWhenFindAllByElementsIsNull() {
        RepositoryData actual = new RepositoryData();
        testSubject.execute(ElementCollection.builder().build(), actual);

        assertThat(actual).isEqualTo(new RepositoryData());
    }

    @Test
    void executeShouldDoNothingWhenFindOneByElementsIsEmpty() {
        RepositoryData actual = new RepositoryData();
        testSubject.execute(ElementCollection.builder().findOneByElements(List.of()).build(), actual);

        assertThat(actual).isEqualTo(new RepositoryData());
    }

    @Test
    void executeShouldAddFindOneByAndGetOneByMethodsForEachFindOneByElementInElementCollection() {
        Element nameField = createMockElement("Name", "String", createFindOneBy(""));
        Element ageField = createMockElement("Age", "Long", createFindOneBy("LessThan"));

        RepositoryData actual = RepositoryData.builder().findOneMethods(new HashSet<>()).findMethods(new HashSet<>()).countMethods(new HashSet<>()).build();
        testSubject.execute(ElementCollection.builder().findOneByElements(List.of(nameField, ageField)).build(), actual);


        assertThat(actual.getFindOneMethods())
                .contains(
                        MethodData.builder().methodName("OneByName").parameters("String arg0").build(),
                        MethodData.builder().methodName("OneByAgeLessThan").parameters("Long arg0").build()
                );
    }

    private FindOneBy createFindOneBy(String modifier) {
        return new FindOneBy() {

            @Override
            public Class<? extends Annotation> annotationType() {
                return FindOneBy.class;
            }

            @Override
            public String modifier() {
                return modifier;
            }
        };
    }

    private Element createMockElement(String fieldName, String typeName, FindOneBy findOneBy) {
        Name name = mock(Name.class);
        when(name.toString()).thenReturn(fieldName);

        TypeMirror typeMirror = mock(TypeMirror.class);
        when(typeMirror.toString()).thenReturn(typeName);

        Element element = mock(Element.class);
        when(element.getSimpleName()).thenReturn(name);
        when(element.asType()).thenReturn(typeMirror);
        when(element.getAnnotation(FindOneBy.class)).thenReturn(findOneBy);
        return element;
    }
}
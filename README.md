# dynamic-repository
Simple annotation processor which can generate [`JpaRepository`](https://docs.spring.io/spring-data/jpa/docs/current/api/org/springframework/data/jpa/repository/JpaRepository.html) interfaces, 
instead of creating them manually.  
Dynamic repository also supports annotation based find (and count), find or get one method definitions,
rest repositories and projections.

Supported annotations:
- [`@DynamicRepository`](#dynamicrepository)
- [`@FindAll`](#findall)
- [`@FindAllBy`](#findallby)
- [`@FindOneBy`](#findoneby)
- [`@Projected`](#projected)

## Usage
For maven projects, just add our dependency:

```xml
<dependency>
    <groupId>io.overcoded</groupId>
    <artifactId>dynamic-repository</artifactId>
    <version>1.1.0</version>
    <scope>compile</scope>
</dependency>
```

If you are using [IntelliJ IDEA](https://www.jetbrains.com/idea/), you can enable annotation processing under  
`Preferences` > `Build, Execution, Deployment` > `Compiler` > `Annotation Processors`. 

### `@DynamicRepository` 
`@DynamicRepository` is a marker annotation wit three (optional) property: 
- `suffix` to define the suffix of the repository interface (default: `JpaRepository`).
- `restResourceEnabled` to expose repository as rest repository
- `resourcePath` to change the default (Spring generated) endpoint of the rest repository

If a domain class is marked with `@DynamicRepository` the processor should generate a [`JpaRepository`](https://docs.spring.io/spring-data/jpa/docs/current/api/org/springframework/data/jpa/repository/JpaRepository.html) for it.

The outcome should be a generated public [`JpaRepository`](https://docs.spring.io/spring-data/jpa/docs/current/api/org/springframework/data/jpa/repository/JpaRepository.html) in the same package with the domain class.

#### Example usage

```java
@Data
@Entity
@DynamicRepository
public class Page {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    private String name;
}
```

Should generate the following interface in the same package.

```java
@Repository
public interface PageJpaRepository extends JpaRepository<Page, Long> {
    <T> List<T> findPagedProjectedBy(Class<T> type);
    <T> List<T> findPagedProjectedBy(Pageable pageable, Class<T> type);
    @Transactional(readOnly = true)
    @QueryHints(value = @QueryHint(name = HINT_FETCH_SIZE, value = "-2147483648"))
    Stream<Page> streamAllBy();
}
```

The `streamAllBy()` method was introduced to support streaming on all entries fetching one row at a time.
The default value is for MySQL, where the hint fetch size should be set to `Integer.MIN_VALUE`. 
Other databases may need different fetch size (e.g. PostgreSQL uses `0`), so this value can be configured in two
different ways:

##### 1. With annotation for a repository
You can set it by configuring the `streamFetchSize` property for `@DynamicRepository`, e.g.: 

```
@DynamicRepository(streamFetchSize="0")
```

##### 2. Globally
If you have a lot of repositories, and you want to change this configuration globally you have to create file 
`src/resources/repository.properties` where you can set `streamFetchSize`, e.g.: 

```
streamFetchSize=0
```

Besides `streamAllBy` method each `findAll` or `findAllBy` method also generates the corresponding `streamAll` method. 

Projections also supported through the `findPagedProjectedBy` methods.

### `@FindAll`
`@FindAll` is a class level, repeatable annotation which responsible to generate `findAll` and `countAll` methods with multiple arguments. 

`@FindAll` annotation has two properties: 
- `method` to define the (find) method
- `types` to define the argument types of this method

Don't forget to annotate your class with `@DynamicRepository` annotation!

#### Example usage

```java
@Data
@Entity
@DynamicRepository
@FindAll(method = "findAllByNameContainingIgnoreCaseAndAgeGreaterThan", types = {String.class, Integer.class}) 
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    private String name;

    @NotBlank
    private String email;

    private Integer age;
}
```

Should generate the following interface in the same package.

```java
@Repository
public interface UserJpaRepository extends JpaRepository<User, Long> {
   List<User> findAllByNameContainingIgnoreCaseAndAgeGreaterThan(String name, Integer age);
   List<User> findAllByNameContainingIgnoreCaseAndAgeGreaterThan(String name, Integer age, Pageable pageable);
   long countAllByNameContainingIgnoreCaseAndAgeGreaterThan(String name, Integer age);
}
```

### `@FindAllBy`
`@FindAllBy` is a field level annotation which responsible to generate `findAll` and `countAll` methods for the specified field. 

`@FindAllBy` annotation has one property: 
- `modifier` to modifier the suffix of a repository method ([see](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods.query-creation))

Don't forget to annotate your class with `@DynamicRepository` annotation!

#### Example usage

```java
@Data
@Entity
@DynamicRepository
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @FindAllBy(modifier = "ContainingIgnoreCase") 
    private String name;

    @NotBlank
    private String email;

    private Integer age;
}
```

Should generate the following interface in the same package.

```java
@Repository
public interface UserJpaRepository extends JpaRepository<User, Long> {
   List<User> findAllByNameContainingIgnoreCase(String name);
   List<User> findAllByNameContainingIgnoreCase(String name, Pageable pageable);
   long countAllByNameContainingIgnoreCase(String name);
}
```

### `@FindOneBy`
`@FindOneBy` is a field level annotation which responsible to generate `findOneBy` and `getOneBy` methods for the specified field.

`@FindOneBy` annotation has one property:
- `modifier` to modifier the suffix of a repository method ([see](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods.query-creation))

Don't forget to annotate your class with `@DynamicRepository` annotation!

#### Example usage

```java
@Data
@Entity
@DynamicRepository
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    private String name;

    @NotBlank
    @FindOneBy
    private String email;

    private Integer age;
}
```

Should generate the following interface in the same package.

```java
@Repository
public interface UserJpaRepository extends JpaRepository<User, Long> {
   User getOneByEmail(String email);
   Optional<User> findOneByEmail(String email);
}
```

### `@Projected`
`@Projected` is a field level annotation which responsible to generate an interface with getters for the specified fields.

`@Projected` annotation currently not supporting generate projection filter methods.
Don't forget to annotate your class with `@DynamicRepository` annotation!

#### Example usage

```java
@Data
@Entity
@DynamicRepository
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Projected
    private String name;

    @NotBlank
    @Projected
    private String email;

    private Integer age;
}
```

Should generate the following interface in the same package.

```java
public interface UserProjection {
   String getName(String name);
   String getEmail(String email);
}
```

You can use this, with the automatically generated projection methods in
the repository: 

```java
@Repository
public interface UserJpaRepository extends JpaRepository<User, Long> {
   <T> List<T> findPagedProjectedBy(Class<T> type);
   <T> List<T> findPagedProjectedBy(Pageable pageable, Class<T> type);
}
```

